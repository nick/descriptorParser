CREATE TABLE IF NOT EXISTS dir_key_certificate(
  header                              TEXT                         NOT NULL,
  dir-key-certificate-version         TEXT                         NOT NULL,
  fingerprint                         TEXT                         NOT NULL,
  dir-key-published                   TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  dir-key-expires                     TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  dir-identity-key                    TEXT                         NOT NULL,
  dir-signing-key                     TEXT                         NOT NULL,
  dir-key-crosscert                   TEXT                         NOT NULL,
  dir-key-certification               TEXT                         NOT NULL,
);
