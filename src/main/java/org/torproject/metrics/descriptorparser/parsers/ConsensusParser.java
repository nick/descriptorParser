package org.torproject.metrics.descriptorparser.parsers;


import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorParseException;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.descriptor.RelayNetworkStatusConsensus;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.Gauge;
import org.torproject.metrics.descriptorparser.utils.OpenMetricsWriter;

import io.prometheus.client.CollectorRegistry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ConsensusParser {
  private static final Logger logger = LoggerFactory.getLogger(
      ConsensusParser.class);

  private static final String INSERT_NETWORK_STATUS_SQL
      = "INSERT INTO"
      + " network_status (header, network_status_version,"
      + " consensus_method, consensus_flavor, valid_after, fresh_until,"
      + " valid_until, vote_seconds, dist_seconds, known_flags,"
      + " recommended_client_version, recommended_server_version,"
      + " recommended_client_protocols, recommended_relay_protocols,"
      + " required_client_protocols, required_relay_protocols,"
      + " params, package_lines, shared_rand_previous_value,"
      + " shared_rand_current_value,"
      + " shared_rand_previous_num, shared_rand_current_num, dir_sources,"
      + " digest, bandwidth_weights, directory_signatures) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?)";

  private static final String INSERT_NETWORK_STATUS_TOTALS_SQL
      = "INSERT INTO"
      + " network_status_totals (total_consensus_weight, total_guard_weight,"
      + " total_middle_weight, total_exit_weight, time, network_status) VALUES "
      + "(?, ?, ?, ?, ?, ?)";

  private static final String INSERT_NETWORK_STATUS_ENTRY_SQL
      = "INSERT INTO network_status_entry"
      + " (nickname, fingerprint, digest, time, ip, or_port,"
      + " dir_port, or_addresses, flags, version, bandwidth_measured,"
      + " bandwidth_unmeasured, bandwidth_weight, proto, policy,"
      + " port_list, master_key_ed25519, network_status) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?)";

  private static final String INSERT_NETWORK_STATUS_ENTRY_WEIGHTS_SQL
      = "INSERT INTO network_status_entry_weights"
      + " (guard_weight, middle_weight, exit_weight, guard_weight_fraction,"
      + " middle_weight_fraction, exit_weight_fraction,"
      + " network_weight_fraction, time, network_status_entry) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?, ?)";

  private static CollectorRegistry registry = new CollectorRegistry();
  private static OpenMetricsWriter opWriter = new OpenMetricsWriter();

  private static final long ONE_HOUR_MILLIS = 60L * 60L * 1000L;

  private static final long ONE_DAY_MILLIS = 24L * ONE_HOUR_MILLIS;

  private static final long ONE_WEEK_MILLIS = 7L * ONE_DAY_MILLIS;

  private static Gauge bwGauge = Gauge.build()
      .name("relay_bandwidth")
      .help("The measured bandwidth in bytes per second.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge descStatusGauge = Gauge.build()
      .name("desc_status")
      .help("Estimated number of intervals when the node was listed as "
          + "running in the network status published by either the "
          + "directory authorities or bridge authority")
      .labelNames("fingerprint", "nickname", "node", "country",
          "transport", "version").register(registry);

  private static Gauge networkWeightGauge = Gauge.build()
      .name("network_weight")
      .help("Metric of a relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge networkFractionGauge = Gauge.build()
      .name("network_fraction")
      .help("Metric of a relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities as a fraction of the total network weight.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge totalNetworkWeightGauge = Gauge.build()
      .name("total_network_weight")
      .help("Metric based on bandwidth observed by relays "
          + "and bandwidth measured by the directory "
          + "authorities.").register(registry);

  private static Gauge networkGuardWeightGauge = Gauge.build()
      .name("network_guard_weight")
      .help("Metric of a guard relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge networkGuardFractionGauge = Gauge.build()
      .name("network_guard_fraction")
      .help("Metric of a guard relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities as a fraction of the total network weight.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge totalNetworkGuardWeightGauge = Gauge.build()
      .name("total_network_guard_weight")
      .help("Metric based on bandwidth observed by guard relays "
          + "and bandwidth measured by the directory "
          + "authorities.").register(registry);

  private static Gauge networkMiddleWeightGauge = Gauge.build()
      .name("network_middle_weight")
      .help("Metric of a middle relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge networkMiddleFractionGauge = Gauge.build()
      .name("network_middle_fraction")
      .help("Metric of a middle relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities as a fraction of the total network weight.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge totalNetworkMiddleWeightGauge = Gauge.build()
      .name("total_network_middle_weight")
      .help("Metric based on bandwidth observed by middle relays "
          + "and bandwidth measured by the directory "
          + "authorities.").register(registry);

  private static Gauge networkExitWeightGauge = Gauge.build()
      .name("network_exit_weight")
      .help("Metric of a exit relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge networkExitFractionGauge = Gauge.build()
      .name("network_exit_fraction")
      .help("Metric of a exit relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities as a fraction of the total network weight.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge totalNetworkExitWeightGauge = Gauge.build()
      .name("total_network_exit_weight")
      .help("Metric based on bandwidth observed by exit relays "
          + "and bandwidth measured by the directory "
          + "authorities.").register(registry);

  private static final Set<String> WEIGHT_KEYS = new HashSet<>(Arrays.asList(
      "Wgg", "Wgd", "Wmg", "Wmm", "Wme", "Wmd", "Wee", "Wed"));

  private static Map<String, ArrayList>
        relayWeights = new HashMap<>();

  private static Map<String, ArrayList>
        relayWeightsFractions = new HashMap<>();

  private double totalConsensusWeight = 0.0;
  private double totalGuardWeight = 0.0;
  private double totalMiddleWeight = 0.0;
  private double totalExitWeight = 0.0;

  /**
   * Parse consensus files and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof RelayNetworkStatusConsensus) {
        RelayNetworkStatusConsensus desc =
            (RelayNetworkStatusConsensus) descriptor;

        double wgg = 0.0;
        double wgd = 0.0;
        double wmg = 0.0;
        double wme = 0.0;
        double wmm = 0.0;
        double wee = 0.0;
        double wmd = 0.0;
        double wed = 0.0;

        boolean consensusContainsBandwidthWeights = false;
        if (desc.getBandwidthWeights() != null
            && (desc.getBandwidthWeights().keySet().containsAll(WEIGHT_KEYS))) {
          consensusContainsBandwidthWeights = true;

          wgg = ((double) desc.getBandwidthWeights().get("Wgg")) / 10000.0;
          wgd = ((double) desc.getBandwidthWeights().get("Wgd")) / 10000.0;
          wmg = ((double) desc.getBandwidthWeights().get("Wmg")) / 10000.0;
          wme = ((double) desc.getBandwidthWeights().get("Wme")) / 10000.0;
          wmm = ((double) desc.getBandwidthWeights().get("Wmm")) / 10000.0;
          wee = ((double) desc.getBandwidthWeights().get("Wee")) / 10000.0;
          wmd = ((double) desc.getBandwidthWeights().get("Wmd")) / 10000.0;
          wed = ((double) desc.getBandwidthWeights().get("Wed")) / 10000.0;
        } else {
          logger.debug("Not calculating new path selection probabilities, "
              + "because we could not determine most recent Wxx parameter "
              + "values, probably because we didn't parse a consensus in "
              + "this execution.");
          continue;
        }

        for (Map.Entry<String, NetworkStatusEntry> e :
            desc.getStatusEntries().entrySet()) {
          String fingerprint = e.getKey();
          NetworkStatusEntry entry = e.getValue();
          boolean isRunning = entry.getFlags().contains("Running");
          double guardWeight = 0.0;
          double middleWeight = 0.0;
          double exitWeight = 0.0;
          double consensusWeight = entry.getBandwidth();
          if (isRunning) {
            boolean isExit = entry.getFlags().contains("Exit")
                && !entry.getFlags().contains("BadExit");
            boolean isGuard = entry.getFlags().contains("Guard");
            this.totalConsensusWeight += consensusWeight;
            if (consensusContainsBandwidthWeights) {
              if (isGuard && isExit) {
                guardWeight = consensusWeight * wgd;
                middleWeight = consensusWeight * wmd;
                exitWeight = consensusWeight * wed;
              } else if (isGuard) {
                guardWeight = consensusWeight * wgg;
                middleWeight = consensusWeight * wmg;
              } else if (isExit) {
                middleWeight = consensusWeight * wme;
                exitWeight = consensusWeight * wee;
              } else {
                middleWeight = consensusWeight * wmm;
              }

            }
            this.totalGuardWeight += guardWeight;
            this.totalMiddleWeight += middleWeight;
            this.totalExitWeight += exitWeight;

            ArrayList tuple = new ArrayList<>();
            tuple.add(consensusWeight);
            tuple.add(guardWeight);
            tuple.add(middleWeight);
            tuple.add(exitWeight);

            this.relayWeights.put(fingerprint, tuple);
          }

        }

        for (Map.Entry<String, ArrayList> e :
            this.relayWeights.entrySet()) {
          String fingerprint = e.getKey();
          ArrayList entry = e.getValue();
          ArrayList tuple = new ArrayList<>();

          double consensusWeight = (double) entry.get(0);
          double guardWeight = (double) entry.get(1);
          double middleWeight = (double) entry.get(2);
          double exitWeight = (double) entry.get(3);

          if (consensusContainsBandwidthWeights) {

            tuple.add((float) (consensusWeight / totalConsensusWeight));
            tuple.add((float) (guardWeight / totalGuardWeight));
            tuple.add((float) (middleWeight / totalMiddleWeight));
            tuple.add((float) (exitWeight / totalExitWeight));
          } else {
            tuple.add((float) consensusWeight / totalConsensusWeight);
            tuple.add((float) 0.0);
            tuple.add((float) 0.0);
            tuple.add((float) 0.0);
          }
          this.relayWeightsFractions.put(
              fingerprint, tuple);
        }

        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());

        this.addNetworkStatus(desc, digest, conn, totalConsensusWeight,
            totalGuardWeight, totalMiddleWeight, totalExitWeight);

        for (Map.Entry<String, NetworkStatusEntry> e :
            desc.getStatusEntries().entrySet()) {
          String fingerprint = e.getKey();
          NetworkStatusEntry entry = e.getValue();
          ArrayList weightsFractions =
              this.relayWeightsFractions.get(fingerprint);
          ArrayList weights = this.relayWeights.get(fingerprint);

          this.addRelayStatus(entry, digest, conn, weights, weightsFractions);
        }

      } else {
        continue;
      }
    }
    opWriter.pushToGateway(registry);
  }

  private void addRelayStatus(NetworkStatusEntry entry,
      String digest, Connection conn,
      ArrayList weights, ArrayList weightsFractions) {

    DescriptorUtils descUtils = new DescriptorUtils();

    double guardWeight = (double) weights.get(1);
    double middleWeight = (double) weights.get(2);
    double exitWeight = (double) weights.get(3);
    float networkFraction = (float) weightsFractions.get(0);
    float guardFraction = (float) weightsFractions.get(1);
    float middleFraction = (float) weightsFractions.get(2);
    float exitFraction = (float) weightsFractions.get(3);

    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_STATUS_ENTRY_SQL);
    ) {

      preparedStatement.setString(1, entry.getNickname());
      preparedStatement.setString(2, entry.getFingerprint());
      String entryDigest = descUtils.calculateDigestSha256Base64(
          entry.getStatusEntryBytes());
      preparedStatement.setString(3, entryDigest);
      preparedStatement.setTimestamp(4,
          new Timestamp(entry.getPublishedMillis()));
      preparedStatement.setString(5, entry.getAddress());
      preparedStatement.setInt(6, entry.getOrPort());
      preparedStatement.setInt(7, entry.getDirPort());
      preparedStatement.setString(8,
          descUtils.fieldAsString(entry.getOrAddresses()));
      preparedStatement.setString(9,
          descUtils.fieldAsString(entry.getFlags()));
      preparedStatement.setString(10, entry.getVersion());
      preparedStatement.setLong(11, entry.getMeasured());
      preparedStatement.setBoolean(12, entry.getUnmeasured());
      preparedStatement.setLong(13, entry.getBandwidth());
      preparedStatement.setString(14,
          descUtils.fieldAsString(entry.getProtocols()));
      preparedStatement.setString(15, entry.getDefaultPolicy());
      preparedStatement.setString(16, entry.getPortList());
      preparedStatement.setString(17, entry.getMasterKeyEd25519());
      preparedStatement.setString(18, digest);
      preparedStatement.executeUpdate();
    } catch (SQLException ex) {
      logger.warn("SQL Exception. {}".format(ex.getMessage()));
    } catch (DescriptorParseException ex) {
      logger.warn("Parsing Exception. {}".format(ex.getMessage()));
    }

    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_STATUS_ENTRY_WEIGHTS_SQL);
    ) {
      preparedStatement.setDouble(1, guardWeight);
      preparedStatement.setDouble(2, middleWeight);
      preparedStatement.setDouble(3, exitWeight);
      preparedStatement.setFloat(4, guardFraction);
      preparedStatement.setFloat(5, middleFraction);
      preparedStatement.setFloat(6, exitFraction);
      preparedStatement.setFloat(7, networkFraction);
      preparedStatement.setTimestamp(8,
          new Timestamp(entry.getPublishedMillis()));
      String entryDigest = descUtils.calculateDigestSha256Base64(
          entry.getStatusEntryBytes());
      preparedStatement.setString(9, entryDigest);
      preparedStatement.executeUpdate();
    } catch (SQLException ex) {
      logger.warn("SQL Exception. {}".format(ex.getMessage()));
    } catch (DescriptorParseException ex) {
      logger.warn("Parsing Exception. {}".format(ex.getMessage()));
    }

    try {
      opWriter.processRequest(bwGauge,
           entry.getFingerprint(), entry.getNickname(), null,
           entry.getPublishedMillis(), entry.getBandwidth());
      if (entry.getFlags().contains("Running")) {
        opWriter.processRouterLabelmetrics(this.descStatusGauge,
            entry.getFingerprint(), entry.getNickname(),
            "relay", "", "", "", entry.getPublishedMillis(), 0.0);
        opWriter.processRequest(this.networkWeightGauge,
            entry.getFingerprint(), entry.getNickname(),
            null, entry.getPublishedMillis(), entry.getBandwidth());
        opWriter.processRequest(this.networkGuardWeightGauge,
            entry.getFingerprint(), entry.getNickname(),
            null, entry.getPublishedMillis(), guardWeight);
        opWriter.processRequest(this.networkMiddleWeightGauge,
            entry.getFingerprint(), entry.getNickname(),
            null, entry.getPublishedMillis(), middleWeight);
        opWriter.processRequest(this.networkExitWeightGauge,
            entry.getFingerprint(), entry.getNickname(),
            null, entry.getPublishedMillis(), exitWeight);
        opWriter.processRequest(this.networkFractionGauge,
            entry.getFingerprint(), entry.getNickname(),
            null, entry.getPublishedMillis(), networkFraction);
        opWriter.processRequest(this.networkGuardFractionGauge,
            entry.getFingerprint(), entry.getNickname(),
            null, entry.getPublishedMillis(), guardFraction);
        opWriter.processRequest(this.networkMiddleFractionGauge,
            entry.getFingerprint(), entry.getNickname(),
            null, entry.getPublishedMillis(), middleFraction);
        opWriter.processRequest(this.networkExitFractionGauge,
            entry.getFingerprint(), entry.getNickname(),
            null, entry.getPublishedMillis(), exitFraction);
      }
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addNetworkStatus(RelayNetworkStatusConsensus desc, String digest,
      Connection conn, double totalConsensusWeight, double totalGuardWeight,
      double totalMiddleWeight, double totalExitWeight) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_STATUS_SQL);
    ) {
      preparedStatement.setString(1, "@type network-status-consensus-3 1.0");
      preparedStatement.setInt(2, desc.getNetworkStatusVersion());
      preparedStatement.setInt(3, desc.getConsensusMethod());
      if (desc.getConsensusFlavor() != null) {
        preparedStatement.setString(4, desc.getConsensusFlavor());
      } else {
        preparedStatement.setString(4, "unflavored");
      }
      preparedStatement.setTimestamp(5,
          new Timestamp(desc.getValidAfterMillis()));
      preparedStatement.setTimestamp(6,
          new Timestamp(desc.getFreshUntilMillis()));
      preparedStatement.setTimestamp(7,
          new Timestamp(desc.getValidUntilMillis()));
      preparedStatement.setLong(8, desc.getVoteSeconds());
      preparedStatement.setLong(9, desc.getDistSeconds());
      preparedStatement.setString(10,
          descUtils.fieldAsString(desc.getKnownFlags()));
      preparedStatement.setString(11,
          String.join(", ", desc.getRecommendedClientVersions()));
      preparedStatement.setString(12,
          String.join(", ", desc.getRecommendedServerVersions()));
      preparedStatement.setString(13,
          descUtils.fieldAsString(desc.getRecommendedClientProtocols()));
      preparedStatement.setString(14,
          descUtils.fieldAsString(desc.getRecommendedRelayProtocols()));
      preparedStatement.setString(15,
          descUtils.fieldAsString(desc.getRequiredClientProtocols()));
      preparedStatement.setString(16,
          descUtils.fieldAsString(desc.getRequiredRelayProtocols()));
      preparedStatement.setString(17,
          descUtils.fieldAsString(desc.getConsensusParams()));
      preparedStatement.setString(18,
          descUtils.fieldAsString(desc.getPackageLines()));
      preparedStatement.setString(19,
          desc.getSharedRandPreviousValue());
      preparedStatement.setString(20,
          desc.getSharedRandCurrentValue());
      preparedStatement.setInt(21,
          desc.getSharedRandPreviousNumReveals());
      preparedStatement.setInt(22,
          desc.getSharedRandCurrentNumReveals());
      preparedStatement.setString(23,
          descUtils.fieldAsString(desc.getDirSourceEntries()));
      preparedStatement.setString(24, digest);
      preparedStatement.setString(25,
          descUtils.fieldAsString(desc.getBandwidthWeights()));
      preparedStatement.setString(26,
          descUtils.fieldAsString(desc.getSignatures()));
      preparedStatement.executeUpdate();

    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_STATUS_TOTALS_SQL);
    ) {
      preparedStatement.setDouble(1, totalConsensusWeight);
      preparedStatement.setDouble(2, totalGuardWeight);
      preparedStatement.setDouble(3, totalMiddleWeight);
      preparedStatement.setDouble(4, totalExitWeight);
      preparedStatement.setTimestamp(5,
          new Timestamp(desc.getValidAfterMillis()));
      preparedStatement.setString(6, digest);
      preparedStatement.executeUpdate();
    }  catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

    try {
      opWriter.processNetworkTotal(this.totalNetworkWeightGauge,
          desc.getValidAfterMillis(), totalConsensusWeight);
      opWriter.processNetworkTotal(this.totalNetworkGuardWeightGauge,
          desc.getValidAfterMillis(), totalGuardWeight);
      opWriter.processNetworkTotal(this.totalNetworkMiddleWeightGauge,
          desc.getValidAfterMillis(), totalMiddleWeight);
      opWriter.processNetworkTotal(this.totalNetworkExitWeightGauge,
          desc.getValidAfterMillis(), totalExitWeight);
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
