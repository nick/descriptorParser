package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BridgeNetworkStatus;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.Gauge;
import org.torproject.metrics.descriptorparser.utils.OpenMetricsWriter;

import io.prometheus.client.CollectorRegistry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Map;

public class BridgeNetworkStatusParser {
  private static final String INSERT_BRIDGE_NETWORK_STATUS_SQL
      = "INSERT INTO"
      + " bridge_network_status (published, fingerprint, flag_thresholds,"
      + " stable_uptime, stable_mtbf, fast_bandwidth, guard_wfu, guard_tk,"
      + " guard_bandwidth_including_exits, guard_bandwidth_excluding_exits,"
      + " enough_mtbf_info, ignore_adv_bws, header, digest) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_BRIDGE_STATUS_SQL
      = "INSERT INTO"
      + " bridge_status (published, fingerprint, nickname, digest,"
      + " network_status, address, or_port, dir_port, or_address,"
      + " flags, bandwidth, policy) VALUES "
      + "(?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      BridgeNetworkStatusParser.class);

  private static CollectorRegistry registry = new CollectorRegistry();
  private static OpenMetricsWriter opWriter = new OpenMetricsWriter();

  private static final long ONE_HOUR_MILLIS = 60L * 60L * 1000L;

  private static final long ONE_DAY_MILLIS = 24L * ONE_HOUR_MILLIS;

  private static final long ONE_WEEK_MILLIS = 7L * ONE_DAY_MILLIS;

  private static Gauge descDirreqV3StatusGauge = Gauge.build()
      .name("desc_dirreq_v3_status")
      .help("Estimated number of intervals when the node was listed as "
          + "running in the network status published by either the "
          + "directory authorities or bridge authority")
      .labelNames("fingerprint", "nickname", "node", "country",
          "transport", "version").register(registry);

  private static Gauge bwGauge = Gauge.build()
      .name("bridge_bandwidth")
      .help("The measured bandwidth in bytes per second.")
      .labelNames("fingerprint", "nickname").register(registry);

  /**
   * Parse bridge network statuses and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();

    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof BridgeNetworkStatus) {
        BridgeNetworkStatus desc = (BridgeNetworkStatus) descriptor;
        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());

        this.addNetworkStatus(desc, digest, conn);

        for (Map.Entry<String, NetworkStatusEntry> e :
            desc.getStatusEntries().entrySet()) {
          String fingerprint = e.getKey();
          NetworkStatusEntry entry = e.getValue();

          this.addBridgeStatus(entry, digest, conn);
        }

      } else {
        // We're only interested in bridge-network-satuses
        continue;
      }
    }
    opWriter.pushToGateway(registry);
  }

  private void addBridgeStatus(NetworkStatusEntry entry,
      String networkStatusDigest, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_BRIDGE_STATUS_SQL);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(entry.getPublishedMillis()));
      preparedStatement.setString(2, entry.getFingerprint());
      preparedStatement.setString(3, entry.getNickname());
      preparedStatement.setString(4, descUtils.calculateDigestSha256Base64(
          entry.getStatusEntryBytes()));
      preparedStatement.setString(5, networkStatusDigest);
      preparedStatement.setString(6, entry.getAddress());
      preparedStatement.setInt(7, entry.getOrPort());
      preparedStatement.setInt(8, entry.getDirPort());
      preparedStatement.setString(9, descUtils.fieldAsString(
          entry.getOrAddresses()));
      preparedStatement.setString(10, descUtils.fieldAsString(
          entry.getFlags()));
      preparedStatement.setLong(11, entry.getBandwidth());
      preparedStatement.setString(12, entry.getDefaultPolicy());
      preparedStatement.executeUpdate();
      opWriter.processRequest(bwGauge,
           entry.getFingerprint(), entry.getNickname(), null,
           entry.getPublishedMillis(), entry.getBandwidth());
      if (entry.getFlags().contains("Running")) {
        long publishedMillis = entry.getPublishedMillis();
        long fromMillis = (publishedMillis / ONE_HOUR_MILLIS)
            * ONE_HOUR_MILLIS;
        long toMillis = fromMillis + ONE_HOUR_MILLIS;
        opWriter.processRouterLabelmetrics(descDirreqV3StatusGauge,
            entry.getFingerprint(), entry.getNickname(),
            "bridge", "", "", "", toMillis, 0.0);
      }
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addNetworkStatus(BridgeNetworkStatus desc, String digest,
        Connection conn) {
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BRIDGE_NETWORK_STATUS_SQL);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setString(2, desc.getFingerprint());
      String flagThresholds = String.format("stable-uptime=%s stable-mtbf=%s"
          + " fast-speed=%s"
          + " guard-wfu=%s guard-tk=%s guard-bw-inc-exits=%s"
          + " guard-bw-exc-exits=%s enough-mtbf=%s"
          + " ignoring-advertised-bws=%s",
          desc.getStableUptime(),
          desc.getStableMtbf(),
          desc.getFastBandwidth(),
          desc.getGuardWfu(),
          desc.getGuardTk(),
          desc.getGuardBandwidthIncludingExits(),
          desc.getGuardBandwidthExcludingExits(),
          desc.getEnoughMtbfInfo(),
          desc.getIgnoringAdvertisedBws());
      preparedStatement.setString(3, flagThresholds);
      preparedStatement.setLong(4, desc.getStableUptime());
      preparedStatement.setLong(5, desc.getStableMtbf());
      preparedStatement.setLong(6, desc.getFastBandwidth());
      preparedStatement.setDouble(7, desc.getGuardWfu());
      preparedStatement.setLong(8, desc.getGuardTk());
      preparedStatement.setLong(9, desc.getGuardBandwidthIncludingExits());
      preparedStatement.setLong(10, desc.getGuardBandwidthExcludingExits());
      preparedStatement.setInt(11, desc.getEnoughMtbfInfo());
      preparedStatement.setInt(12, desc.getIgnoringAdvertisedBws());
      /*
       * We can hardcode the header for now, but it would be better to add
       * the parsing logic to metrics lib in case this change
       **/
      preparedStatement.setString(13, "@type bridge-network-status 1.2");
      preparedStatement.setString(14, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
