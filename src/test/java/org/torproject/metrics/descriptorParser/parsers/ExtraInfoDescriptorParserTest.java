package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.torproject.descriptor.BridgeExtraInfoDescriptor;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.RelayExtraInfoDescriptor;
import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.io.File;

import java.lang.reflect.Method;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class ExtraInfoDescriptorParserTest {

  private static final long ONE_HOUR_MILLIS = 60L * 60L * 1000L;
  private static final long ONE_DAY_MILLIS = 24L * ONE_HOUR_MILLIS;

  @Test()
  public void testExtraInfoDescriptorParserDbUploader() throws Exception {
    ExtraInfoDescriptorParser ep = new ExtraInfoDescriptorParser();
    String extraInfoPath =
        "src/test/resources/2022-08-30-16-05-00-extra-infos";
    String confFile = "src/test/resources/config.properties.test";
    String extraInfoDigest = "142ca315888f558a7ab99477fc4928b559988cf5";
    String fingerprint = "BA77149B4EDA76543698F05104F5C2547E306D77";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    ep.run(extraInfoPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM extra_info_descriptor WHERE digest_sha1_hex = '"
        + extraInfoDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("digest_sha1_hex"), extraInfoDigest);
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
      }
    }

  }

  @Test()
  public void testBandwidthHistory() throws Exception {
    ExtraInfoDescriptorParser ep = new ExtraInfoDescriptorParser();
    String extraInfoPath =
        "src/test/resources/extra-info-bw";
    String confFile = "src/test/resources/config.properties.test";
    String extraInfoDigest = "142ca315888f558a7ab99477fc4928b559988cf5";
    String fingerprint = "BA77149B4EDA76543698F05104F5C2547E306D77";

    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(extraInfoPath))) {
      if ((descriptor instanceof RelayExtraInfoDescriptor)) {
        RelayExtraInfoDescriptor desc = (RelayExtraInfoDescriptor) descriptor;
        assertTrue(descriptor instanceof RelayExtraInfoDescriptor);
        String readBandwidthHistory = desc.getReadHistory().getLine();
        String[] parts = readBandwidthHistory.split(" ");

        Method privateParseIntervalString =
            ExtraInfoDescriptorParser.class.getDeclaredMethod(
            "parseIntervalString",
            String.class);
        privateParseIntervalString.setAccessible(true);
        long intervalLength =
            (long) privateParseIntervalString.invoke(
                ep, parts[3].substring(1));
        assertEquals(intervalLength,
            Long.parseLong(parts[3].substring(1)) * 1000);

        Method privateExtractBandwidthValue =
            ExtraInfoDescriptorParser.class.getDeclaredMethod(
            "extractBandwidthValue",
            long.class, String.class);
        privateExtractBandwidthValue.setAccessible(true);
        String[] values = parts[5].split(",");
        String value = values[0];
        double bandwidthValue =
            (double) privateExtractBandwidthValue.invoke(
                ep, intervalLength, value);
        double epsilon = 0.001d;
        assertEquals(bandwidthValue, 4016962.998, epsilon);

      }
    }

  }

  @Test()
  public void testSnowflakeTransportStats() throws Exception {
    ExtraInfoDescriptorParser ep = new ExtraInfoDescriptorParser();
    String extraInfoPath =
        "src/test/resources/flakeys-extra-info";
    String confFile = "src/test/resources/config.properties.test";

    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(extraInfoPath))) {
      if ((descriptor instanceof BridgeExtraInfoDescriptor)) {
        BridgeExtraInfoDescriptor desc = (BridgeExtraInfoDescriptor) descriptor;
        assertTrue(descriptor instanceof BridgeExtraInfoDescriptor);

        long dirreqStatsEndMillis = desc.getDirreqStatsEndMillis();
        long dirreqStatsIntervalLengthMillis =
            desc.getDirreqStatsIntervalLength();

        SortedMap<String, Integer> responses = desc.getDirreqV3Resp();
        SortedMap<String, Integer> requests = desc.getDirreqV3Reqs();
        SortedMap<String, Integer> bridgeIpTransports =
            desc.getBridgeIpTransports();
        long statsStartMillis = dirreqStatsEndMillis
            - dirreqStatsIntervalLengthMillis;

        double resp = ((double) responses.get("ok")) - 4.0;
        double total = 0.0;
        if (resp > 0.0) {
          if (statsStartMillis >= dirreqStatsEndMillis) {
            return;
          }
          SortedMap<String, Double> frequenciesCopy = new TreeMap<>();
          if (bridgeIpTransports != null) {
            for (Map.Entry<String, Integer> e :
                bridgeIpTransports.entrySet()) {
              if (e.getValue() < 4.0) {
                continue;
              }
              double frequency = ((double) e.getValue()) - 4.0;
              frequenciesCopy.put(e.getKey(), frequency);
              total += frequency;
            }
          }
          if (frequenciesCopy.isEmpty()) {
            frequenciesCopy.put("<OR>", 4.0);
            total = 4.0;
          }


          for (Map.Entry<String, Double> e : frequenciesCopy.entrySet()) {
            double val = e.getValue() / total * resp;

            Method privateComputeFrequencyValue =
                ExtraInfoDescriptorParser.class.getDeclaredMethod(
                    "computeFrequencyValue",
                    double.class, double.class, double.class, double.class);
            privateComputeFrequencyValue.setAccessible(true);
            double testVal = (double) privateComputeFrequencyValue.invoke(
                ep, resp, e.getValue(), total);

            assertEquals(String.valueOf(val), String.valueOf(testVal));
          }

        }

      }
    }


  }

}
