package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BridgeNetworkStatusParserTest {

  @Test()
  public void testBridgeNetworkStatusParserDbUploader() throws Exception {
    BridgeNetworkStatusParser bp = new BridgeNetworkStatusParser();
    String bridgeNetworkStatusPath =
        "src/test/resources/20220830-135407-statuses";
    String confFile = "src/test/resources/config.properties.test";
    String bridgeNetworkStatusDigest =
        "cikkwS/ea6cFC66cwCc1toDdkHJ3wxVg7ItHpjj//nA";

    String serge = "BA44A889E64B93FAA2B114E02C2A279A8555C533";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(bridgeNetworkStatusPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bridge_network_status WHERE digest = '"
        + bridgeNetworkStatusDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("digest"), bridgeNetworkStatusDigest);
        assertEquals(rs.getString("fingerprint"), serge);
      }
    }
  }

}
