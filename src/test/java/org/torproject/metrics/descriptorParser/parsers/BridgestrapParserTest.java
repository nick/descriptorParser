package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BridgestrapParserTest {

  @Test()
  public void testBridgestrapParserDbUploader() throws Exception {
    BridgestrapParser bp = new BridgestrapParser();
    String bridgestrapPath =
        "src/test/resources/2022-08-29-19-22-41-bridgestrap-stats";
    String confFile = "src/test/resources/config.properties.test";
    String bridgestrapDigest = "8mbnh2MceLiTSNwAFNAcB8xzVqwiz9/i/OP7eVDa3PQ";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(bridgestrapPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bridgestrap_stats WHERE digest = '"
        + bridgestrapDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("digest"), bridgestrapDigest);
        assertEquals(rs.getString("header"),
            "@type bridgestrap-stats 1.0");
      }
    }
  }

}
